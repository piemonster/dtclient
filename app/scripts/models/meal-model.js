dtfront.Models.MealModel = Backbone.RelationalModel.extend({
    relations: [{
        type: Backbone.HasMany,
        key: 'parts',
        relatedModel: 'dtfront.Models.PartModel',
        autoFetch: true,
        reverseRelation: {
            key: 'meal',
            includeInJSON: true,
        },
    },
    {
        type: Backbone.HasMany,
        key: 'guests',
        relatedModel: 'dtfront.Models.GuestModel',
        autoFetch: true,
        reverseRelation: {
            key: 'meal',
            includeInJSON: true,
        },
    }]
});

dtfront.Models.PartModel = Backbone.RelationalModel.extend({
    urlRoot: '/api/mealparts/',
    idAttribute: 'id',

});

dtfront.Models.GuestModel = Backbone.RelationalModel.extend({
    urlRoot: '/api/guests/',
    idAttribute: 'id',

});

// Meal list

dtfront.Views.MealListView = Backbone.View.extend({

    tagName:'ul',

    initialize:function () {
        this.model.bind("reset", this.render, this);
    },

    render:function (eventName) {
        _.each(this.model.models, function (meal) {
            $(this.el).append(new dtfront.Views.MealListItemView({model:meal}).render().el);
        }, this);
        return this;
    },
    attributes : function () {
    return {
      class : "nav nav-list"
    };
  }
  // attributes

});

// Meal List Item

dtfront.Views.MealListItemView = Backbone.View.extend({

    tagName:"li",

    template:_.template($('#tpl-meal-list-item').html()),

    render:function (eventName) {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }

});

// Meal Detail

dtfront.Views.MealView = Backbone.View.extend({

    template:_.template($('#tpl-meal-details').html()),

    render:function (eventName) {
        $(this.el).html(this.template(this.model.toJSON()));
        console.log(this.model.toJSON());
        return this;
    }

});
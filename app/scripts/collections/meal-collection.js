dtfront.Collections.MealCollection = Backbone.Collection.extend({

  model: dtfront.Models.MealModel,
  url:'/api/meals/'

});

dtfront.Collections.PartCollection = Backbone.Collection.extend({

  model: dtfront.Models.PartModel,
  url:'/api/mealparts/'

});

dtfront.Collections.GuestCollection = Backbone.Collection.extend({

  model: dtfront.Models.GuestModel,
  url:'/api/guests/'

});


window.dtfront = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  init: function() {
    console.log('Hello from Backbone!');
  }
};

$(document).ready(function(){
  dtfront.init();

  var app = new dtfront.Routers.ApplicationRouter();
  Backbone.history.start();

});

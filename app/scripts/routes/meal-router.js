dtfront.Routers.ApplicationRouter = Backbone.Router.extend({

    routes:{
        "":"list",
        "meals/:id":"mealDetails",
        "meals/find":"mealFind",
    },

    list:function () {
        this.mealList = new dtfront.Collections.MealCollection();
        this.mealListView = new dtfront.Views.MealListView({model:this.mealList});  // sort out the right view for this
        this.mealList.fetch();  // fetch the items
        console.log(this.mealList);
        $('#sidebar').html(this.mealListView.render().el);  // display the results in html (replace with new template stuff)
    },

    mealDetails:function (id) {
        // this.meal = this.mealList.get(id);
        // this.mealView = new dtfront.Views.MealView({model:this.meal});
        // $('#content').html(this.mealView.render().el);

        if (this.mealList) {
            this.meal = this.mealList.get(id);
            if (this.mealView) this.mealView.remove(); // should add an unbind here as well
            this.mealView = new dtfront.Views.MealView({model:this.meal});
            $('#content').html(this.mealView.render().el);
        } else {
            this.requestedId = id;
            this.list();
        }
    },
    
    mealFind:function (id) {
        // this.meal = this.mealList.get(id);
        // this.mealView = new dtfront.Views.MealView({model:this.meal});
        // $('#content').html(this.mealView.render().el);

        if (this.mealList) {
            this.meal = this.mealList.get(id);
            if (this.mealView) this.mealView.remove(); // should add an unbind here as well
            this.mealView = new dtfront.Views.MealView({model:this.meal});
            $('#content').html(this.mealView.render().el);
        } else {
            this.requestedId = id;
            this.list();
        }
    },

    
});
